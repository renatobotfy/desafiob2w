from flask_pymongo import ObjectId
from app import app
from app.database.dbAccess import dbAccess

@app.route('/getplanetbyid/<string:id>', methods=['GET'])
def getPlanetById(id):
  db = dbAccess(app)
  try:
    queryResponse = db.get_one({'_id': ObjectId(id)})
    response = processGetPlanetByIdResponse(queryResponse)
  except Exception as err:
    print(err)
    response = {"msg": err}
  
  return response

def processGetPlanetByIdResponse(response):
  print(response)
  if response:
    if "_id" in response:
      id = str(response["_id"])
      response["_id"] = id
      return response
    else:
      return ({"msg": "planet not found"})
  else:
      return ({"msg": "none"})