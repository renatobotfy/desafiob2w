from flask import jsonify
from app import app
from app.database.dbAccess import dbAccess

@app.route('/getallplanets', methods=['GET'])
def getAllPlanets():
  db = dbAccess(app)
  try:
    queryResponse = db.get_all()
    response = processGetAllPlanetsResponse(queryResponse)
  except Exception as err:
    print(err)
    response = {"msg": err}
  
  return response

def processGetAllPlanetsResponse(response):
  data = list()
  if response.count() > 0:
    for item in response:
      planetItem = dict()
      if '_id' in item:
        planetItem['_id'] = str(item['_id'])
      if 'name' in item:
        planetItem['name'] = item['name']
      if 'terrain' in item:
        planetItem['terrain'] = item['terrain']
      if 'climate' in item:
        planetItem['climate'] = item['climate']
      if 'numberOfAppearances' in item:
        planetItem['numberOfAppearances'] = item['numberOfAppearances']
      data.append(planetItem)

    return jsonify({"size": response.count(), "data": data})
  else:
    return ({"msg": "no planets found"})