from flask import request
from app import app
from app.database.dbAccess import dbAccess

@app.route('/getplanetbyname/<string:name>', methods=['GET'])
def getPlanetByName(name):
  db = dbAccess(app)
  try:
    queryResponse = db.get_one({"name":name})
    response = processGetPlanetByNameResponse(queryResponse)
  except Exception as err:
    print(err)
    response = {"msg": err}
  
  return response

def processGetPlanetByNameResponse(response):
  print(response)
  if response:
    if "_id" in response:
      id = str(response["_id"])
      response["_id"] = id
      return response
    else:
      return ({"msg": "planet not found"})
  else:
      return ({"msg": "none"})