import os
from flask_pymongo import PyMongo, ObjectId

mongo = None

class dbAccess():
  def __init__(self, app):
    self.app = app

  def __get_mongo(self):
    global mongo

    if (self.app.config['ENV'] == 'test'):
      #print("<<< TEST ENV >>>")
      self.app.config['MONGO_URI'] = 'mongodb+srv://testUser:testPasswd@cluster0-utnyd.mongodb.net/test_planetsDB?retryWrites=true&w=majority'
    elif (self.app.config['ENV'] == 'development'):
      #print("<<< DEV ENV >>>")
      self.app.config['MONGO_URI'] = 'mongodb+srv://testUser:testPasswd@cluster0-utnyd.mongodb.net/test_planetsDB?retryWrites=true&w=majority'
    elif (self.app.config['ENV'] == 'production'):
      #print("<<< PROD ENV >>>")
      self.app.config['MONGO_URI'] = 'mongodb+srv://planetUser1:planetPasswd1@cluster0-utnyd.mongodb.net/planetsDB?retryWrites=true&w=majority'

    if not mongo:
      mongo = PyMongo(self.app)

    return mongo

  def get_one(self, data):
    return self.__get_mongo().db.planetsCollection.find_one(data)

  def get_all(self):
    return self.__get_mongo().db.planetsCollection.find({})
  
  def add_one(self, data):
    return self.__get_mongo().db.planetsCollection.insert_one(data)

  def remove_one(self, id):
    return self.__get_mongo().db.planetsCollection.delete_one({"_id": ObjectId(id)})