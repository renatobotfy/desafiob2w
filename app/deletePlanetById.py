from flask import jsonify
from app import app
from app.database.dbAccess import dbAccess

@app.route('/deleteplanetbyid/<string:id>', methods=['DELETE'])
def deletePlanetById(id):
  db = dbAccess(app)
  try:
    queryResponse = db.remove_one(id)
    response = processDeletePlanetByIdResponse(queryResponse)
  except Exception as err:
    print(err)
    response = {"msg": err}
  return response

def processDeletePlanetByIdResponse(response):
  if response.deleted_count > 0:
    return jsonify({"deleted": response.deleted_count})
  else:
    return ({"msg": "planet not found"})
