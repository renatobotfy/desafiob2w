from flask import request
from app import app
from app.database.dbAccess import dbAccess
import requests

@app.route('/addplanet', methods=['POST'])
def addPlanet():
  db = dbAccess(app)

  url = 'https://swapi.co/api/planets/?search={}'.format(request.json['name'])
  swapiResponse = requests.get(url).json()
  if 'count' in swapiResponse:
    if swapiResponse['count'] > 0:
      for item in swapiResponse['results']:
        if item['name'].lower() == request.json['name'].lower():
          numberOfAppearances = len(item['films'])
          planet = {
            'name': request.json['name'],
            'climate': request.json['climate'],
            'terrain': request.json['terrain'],
            'numberOfAppearances': numberOfAppearances
          }
          print(planet)
          try:
            queryResponse = db.add_one(planet)
            response = processAddPlanetResponse(queryResponse, planet)
          except Exception as err:
            print(err)
            response = {"msg": err}
          break
  return response

def processAddPlanetResponse(response, planet):
  planet["_id"] = str(response.inserted_id)
  return planet