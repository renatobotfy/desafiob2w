# SWPlanets API

## Info

Just another REST API to manage a MongoDB Star Wars' planets list :S

## Installing project dependencies

First, install `pipenv` (and python and pip, if you need), then create a virtual env running `pipenv shell` and then run `pipenv sync` in order to install all depencencies.  
>Please note that you should run these last two commands on project root folder.

## Starting

Run `flask run` on project root folder to start a development server.  
> Do not forget to change environments in `.flaskenv`

### ENV

Change the environment in `.flaskenv` accordingly to your needs.  
Please note that there are only three possible values, otherwise an error will occur

- production
- development
- test

Example

``` config
FLASK_ENV=production
```

## Usage (routes)

### Add a planet

To add a planet, make a `POST` request to this endpoint

``` http
[POST] localhost:5000/addPlanet
```

with a `json` body with the planet's `name`, `climate` and `terrain`

``` json
[Content-Type: application/json]
{
  "name": "Tatooine",
  "climate": "arid",
  "terrain": "desert"
}
```

As response, the inserted object will be returned

``` json
{
  "_id": "5d9e64801ef508ddad12835e",
  "climate": "arid",
  "name": "Tatooine",
  "numberOfAppearances": 5,
  "terrain": "desert"
}
```

>All endpoints must be accessed through `localhost:5000`, and this will be omitted for simplicity

### Delete a planet

To delete a planet, make a `DELETE` request to this endpoint, passing the `_id` of the planet (document) to be deleted

``` http
[DELETE] deleteplanetbyid/<string:id>
```

As response, a delete count will be returned. If it is zero, no document was found

``` json
{
  "deleted": 1
}
```

### Retrieve all planets

To retrieve all planets, make a `GET` request to this endpoint

``` http
[GET] getAllPlanets
```

As response, a object will be returned. In its field `data` will show up all planets added so far, and its field `size` will tell the length of the `data` field.

``` json
{
  "data": [
    {
      "_id": "5d9e64801ef508ddad12835e",
      "climate": "arid",
      "name": "Tatooine",
      "numberOfAppearances": 5,
      "terrain": "desert"
    },
    {
      "_id": "5d9e684a1ef508ddad12835f",
      "climate": "unknown",
      "name": "Jakku",
      "numberOfAppearances": 1,
      "terrain": "deserts"
    }
  ],
  "size": 2
}
```

### Home

To make you smile, this endpoint yelds a "Hello World"

``` http
[GET] /
```

Response

``` json
{
  "msg": "Hello World"
}
```

### Retrive planet by Id

To retrieve a planets by its `id`, make a `GET` request to this endpoint passing the document's id

``` http
[GET] getplanetbyid/<string:id>
```

As response, a object will be returned

``` json
{
  "_id": "5d9e64801ef508ddad12835e",
  "climate": "arid",
  "name": "Tatooine",
  "numberOfAppearances": 5,
  "terrain": "desert"
}
```

### Retrive planet by name

To retrieve a planets by its `name`, make a `GET` request to this endpoint passing the planet's name

``` http
[GET] getplanetbyname/<string:name>
```

As response, a object will be returned

``` json
{
  "_id": "5d9e64801ef508ddad12835e",
  "climate": "arid",
  "name": "Tatooine",
  "numberOfAppearances": 5,
  "terrain": "desert"
}
```

## Testing

Run `python -m unittest discover` to run the test suite.  
