from app import app
import unittest, json

class AppTest(unittest.TestCase):
  def setUp(self):
    app.testing = True
    self.app = app.test_client()
    app.config["ENV"] = 'test'

  def tearDown(self):
    pass

  def addAPlanet(self):
    planet = {
      "name":    "Dagobah",
      "climate": "murky",
      "terrain": "swamp, jungles"
    }
    headers = [('Content-Type', 'application/json')]
    return self.app.post('/addplanet', data=json.dumps(planet), headers=headers)

  def deleteAPlanet(self, response):
    id = response["_id"]
    return self.app.delete('/deleteplanetbyid/{}'.format(id))

  def test_getHome(self):
    response = self.app.get('/')
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data, b'{"msg":"Hello World"}\n')

  def test_addPlanet(self):
    response = self.addAPlanet()
    self.deleteAPlanet(json.loads(response.get_data(as_text=True)))
    self.assertEqual(response.status_code, 200)
  
  def test_deletePlanetById(self):
    response = self.addAPlanet()
    responseDel = self.deleteAPlanet(json.loads(response.get_data(as_text=True)))
    self.assertEqual(responseDel.status_code, 200)

  def test_getPlanetByName(self):
    responseAdd = self.addAPlanet()
    responseAdd_json = json.loads(responseAdd.get_data(as_text=True))
    name = responseAdd_json['name']

    response = self.app.get('/getplanetbyname/{}'.format(name))
    response_json = json.loads(response.get_data(as_text=True))
    self.deleteAPlanet(responseAdd_json)
    self.assertEqual(response_json['name'], name)

  def test_getPlanetById(self):
    responseAdd = self.addAPlanet()
    responseAdd_json = json.loads(responseAdd.get_data(as_text=True))
    id = responseAdd_json['_id']

    response = self.app.get('/getplanetbyid/{}'.format(id))
    response_json = json.loads(response.get_data(as_text=True))
    self.deleteAPlanet(responseAdd_json)
    self.assertEqual(response_json['_id'], id)