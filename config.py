class Config(object):
  DEBUG = False
  TESTING = False

class ProductionConfig(Config):
  MONGO_URI = 'mongodb+srv://planetUser1:planetPasswd1@cluster0-utnyd.mongodb.net/planetsDB?retryWrites=true&w=majority'

class DevelopmentConfig(Config):
  DEBUG = True
  MONGO_URI = 'mongodb+srv://testUser:testPasswd@cluster0-utnyd.mongodb.net/test_planetsDB?retryWrites=true&w=majority'

class TestingConfig(Config):
  TESTING = True
  MONGO_URI = 'mongodb+srv://testUser:testPasswd@cluster0-utnyd.mongodb.net/test_planetsDB?retryWrites=true&w=majority'
